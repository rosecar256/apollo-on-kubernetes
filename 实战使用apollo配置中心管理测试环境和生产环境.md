#### 1.实战维护多套dubbo微服务环境(暂时只部署环境,dubbo后面再引入,会按照DEV,FAT,UAT,PRO的方式来部署)，请根据自己虚拟机负荷量选择性部署。

##### apollo的github参考地址：https://github.com/ctripcorp/apollo/tree/master/scripts/apollo-on-kubernetes



##### 1.1：生产实践（其它数据库部分参考Apollo交付至kubernetes集群文档）

```tex
迭代新需求/修复BUG（编码->提GIT）
测试环境发版，测试（应用通过编译打包发布至TEST命名空间）
测试通过，上线（应用镜像直接发布至PROD命名空间）
```

- K8S内系统架构

| 开发环境（DEV）   | dev  | apollo-config，apollo-admin |
| ----------------- | ---- | --------------------------- |
| 测试环境（TEST）  | fat  | apollo-config，apollo-admin |
| 预发布环境（UAT） | uat  | apollo-config，apollo-admin |
| 生产环境（PROD）  | prod | apollo-config，apollo-admin |

##### 1.2：Apollo的k8s应用配置(删除app命名空间内应用，创建test，dev，uat，prod四套环境命名空间)

```shell
root@k8s-master01:/data/k8s-yaml# kubectl delete ns infra
root@k8s-master01:/data/k8s-yaml# kubectl create ns test
root@k8s-master01:/data/k8s-yaml# kubectl create ns dev
root@k8s-master01:/data/k8s-yaml# kubectl create ns prod
root@k8s-master01:/data/k8s-yaml# kubectl create ns uat
```

```tex
删除infra命名空间内apollo-configservice，apollo-adminservice应用
数据库内删除ApolloConfigDB，创建ApolloConfigTestDB，创建ApolloConfigProdDB

强制删除Terminating的名称空间
root@k8s-master01:/data/k8s-yaml# kubectl get namespace infra -o json >tmp.json

# 此处配置删空
root@k8s-master01:/data/k8s-yaml# vi tmp.json
   "spec": {
        "finalizers": [
        ]
    },
    
# 开启proxy代理apiserver.使用http方式进行删除
root@k8s-master01:/data/k8s-yaml# kubectl proxy 8001

curl -k -H "Content-Type: application/json" -X PUT --data-binary @tmp.json http://127.0.0.1:8001/api/v1/namespaces/infra/finalize
```

##### 1.3：Dev环境SQL修改和导入以及授权操作

```sql
https://github.com/ctripcorp/apollo/blob/master/scripts/apollo-on-kubernetes/db/config-db-dev/apolloconfigdb.sql
root@k8s-master03:~# wget https://raw.githubusercontent.com/ctripcorp/apollo/master/scripts/apollo-on-kubernetes/db/config-db-dev/apolloconfigdb.sql
root@k8s-master03:~# mysql -uroot -pcurry123456 < apolloconfigdb.sql
mysql> update DevApolloConfigDB.ServerConfig set ServerConfig.Value="http://statefulset-apollo-config-server-dev-0.service-apollo-meta-server-dev:8080/eureka/";     # 修改eureka的连接地址,此处用的是service的方式连接。


mysql> grant INSERT,DELETE,UPDATE,SELECT on DevApolloConfigDB.* to "apolloconfig"@"172.18.18.%";     # 使用之前创建的账号做授权,没有的话自己创建此用户
mysql> flush privileges;
```

##### 1.4：Test(**test-alpha**)环境SQL修改和导入以及授权操作

```sql
root@k8s-master03:~# rm -rf apolloconfigdb.sql
https://github.com/ctripcorp/apollo/blob/master/scripts/apollo-on-kubernetes/db/config-db-test-alpha/apolloconfigdb.sql
wget https://raw.githubusercontent.com/ctripcorp/apollo/master/scripts/apollo-on-kubernetes/db/config-db-test-alpha/apolloconfigdb.sql
root@k8s-master03:~# mysql -uroot -pcurry123456 < apolloconfigdb.sql

update TestAlphaApolloConfigDB.ServerConfig set ServerConfig.Value="http://statefulset-apollo-config-server-test-alpha-0.service-apollo-meta-server-test-alpha:8080/eureka/";

mysql> grant INSERT,DELETE,UPDATE,SELECT on TestAlphaApolloConfigDB.* to "apolloconfig"@"172.18.18.%";
```

##### 1.5：Uat(test-beta)环境SQL修改和导入以及授权操作

```sql
root@k8s-master03:~# rm -rf apolloconfigdb.sql
https://github.com/ctripcorp/apollo/blob/master/scripts/apollo-on-kubernetes/db/config-db-test-beta/apolloconfigdb.sql
root@k8s-master03:~# wget https://raw.githubusercontent.com/ctripcorp/apollo/master/scripts/apollo-on-kubernetes/db/config-db-test-beta/apolloconfigdb.sql
root@k8s-master03:~# mysql -uroot -pcurry123456 < apolloconfigdb.sql

update TestBetaApolloConfigDB.ServerConfig set ServerConfig.Value="http://statefulset-apollo-config-server-test-beta-0.service-apollo-meta-server-test-beta:8080/eureka/";


mysql> grant INSERT,DELETE,UPDATE,SELECT on TestBetaApolloConfigDB.* to "apolloconfig"@"172.18.18.%";
```

##### 1.6：Prod环境SQL修改和导入以及授权操作

```sql
root@k8s-master03:~# rm -rf apolloconfigdb.sql
https://github.com/ctripcorp/apollo/blob/master/scripts/apollo-on-kubernetes/db/config-db-prod/apolloconfigdb.sql
root@k8s-master03:~# wget https://raw.githubusercontent.com/ctripcorp/apollo/master/scripts/apollo-on-kubernetes/db/config-db-prod/apolloconfigdb.sql
root@k8s-master03:~# mysql -uroot -pcurry123456 < apolloconfigdb.sql

mysql> update ProdApolloConfigDB.ServerConfig set ServerConfig.Value="http://statefulset-apollo-config-server-prod-0.service-apollo-meta-server-prod:8080/eureka/"; # 生产环境可以根据情况调整eureka的副本数量后,数据库处记得也要同步更新


mysql> grant INSERT,DELETE,UPDATE,SELECT on ProdApolloConfigDB.* to "apolloconfig"@"172.18.18.%";
```

##### 1.7：apollo-config/apollo-admin的configmap配置要点及portal数据库-分环境

```sql
mysql> update ApolloPortalDB.ServerConfig set Value='dev, fat, uat, pro' where Id=1;

root@k8s-master01:/data/k8s-yaml# cp -r apollo-configservice/ apollo-configservice-dev
root@k8s-master01:/data/k8s-yaml# cp -r apollo-configservice/ apollo-configservice-test
root@k8s-master01:/data/k8s-yaml# cp -r apollo-configservice/ apollo-configservice-uat
root@k8s-master01:/data/k8s-yaml# cp -r apollo-configservice/ apollo-configservice-prod

root@k8s-master01:/data/k8s-yaml# cp -r  apollo-adminservice/ apollo-adminservice-dev
root@k8s-master01:/data/k8s-yaml# cp -r  apollo-adminservice/ apollo-adminservice-test
root@k8s-master01:/data/k8s-yaml# cp -r  apollo-adminservice/ apollo-adminservice-uat
root@k8s-master01:/data/k8s-yaml# cp -r  apollo-adminservice/ apollo-adminservice-prod
```



#### 2. 每套环境的yaml配置清单调整流程,配置清单较多,会推到gitlab上,此实验仅以一套dev环境的内容做说明

##### 2.1：apollo-configservice-dev环境配置清单内容

- ##### Mysql-Endpoints配置清单

```yaml
root@k8s-master01:/data/k8s-yaml/apollo-configservice-mysql-dev# pwd
/data/k8s-yaml/apollo-configservice-mysql-dev
root@k8s-master01:/data/k8s-yaml/apollo-configservice-mysql-dev# cat service-mysql-for-apollo-dev-env.yaml 
#
# Copyright 2021 Apollo Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
---
# 为外部 mysql 服务设置 service
kind: Service
apiVersion: v1
metadata:
  namespace: dev
  name: service-mysql-for-apollo-dev-env
  labels:
    app: service-mysql-for-apollo-dev-env
spec:
  ports:
    - protocol: TCP
      port: 3306
      targetPort: 3306
  type: ClusterIP
  sessionAffinity: None

---
kind: Endpoints
apiVersion: v1
metadata:
  namespace: dev
  name: service-mysql-for-apollo-dev-env
subsets:
  - addresses:
      - ip: 172.18.18.20
    ports:
      - protocol: TCP
        port: 3306

```

- ##### configmap-yaml配置清单

```yaml
root@k8s-master01:/data/k8s-yaml/apollo-configservice-dev# pwd
/data/k8s-yaml/apollo-configservice-dev
root@k8s-master01:/data/k8s-yaml/apollo-configservice-dev# cat cm.yaml 
apiVersion: v1
kind: ConfigMap
metadata:
  namespace: dev
  name: configmap-apollo-config-server-dev
data:
  application-github.properties: |
    spring.datasource.url = jdbc:mysql://service-mysql-for-apollo-dev-env:3306/DevApolloConfigDB?characterEncoding=utf8
    spring.datasource.username = apolloconfig
    spring.datasource.password = 123456
    eureka.service.url = http://statefulset-apollo-config-server-dev-0.service-apollo-meta-server-dev:8080/eureka/
  app.properties: |
    appId=100003171
```

- ##### ingress-yaml配置清单

```yaml
root@k8s-master01:/data/k8s-yaml/apollo-configservice-dev# cat ingress.yaml 
apiVersion: networking.k8s.io/v1beta1    
kind: Ingress
metadata: 
  annotations:
    kubernetes.io/ingress.class: "nginx"
  namespace: dev
  name: ingress-apollo-config-server-dev
spec:
  rules:
  - host: dev.linux.com
    http:
      paths:
      - path: /
        backend: 
          serviceName: service-apollo-config-server-dev
          servicePort: 8080
```

- ##### statefulset-yaml配置清单

```yaml
root@k8s-master01:/data/k8s-yaml/apollo-configservice-dev# cat statefulset.yaml 
---
kind: StatefulSet
apiVersion: apps/v1
metadata:
  namespace: dev
  name: statefulset-apollo-config-server-dev
  labels:
    app: statefulset-apollo-config-server-dev
spec:
  serviceName: service-apollo-meta-server-dev
  replicas: 1
  selector:
    matchLabels:
      app: pod-apollo-config-server-dev
  updateStrategy:
    type: RollingUpdate
  template:
    metadata:
      labels:
        app: pod-apollo-config-server-dev
    spec:
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: app
                  operator: In
                  values:
                  - pod-apollo-config-server-dev
              topologyKey: kubernetes.io/hostname

      volumes:
        - name: volume-configmap-apollo-config-server-dev
          configMap:
            name: configmap-apollo-config-server-dev
            #items:
            #  - key: application-github.properties
            #    path: application-github.properties
      
      containers:
        - image: harbor.od.com/infra/apollo-configservice:v1.7.2 
          imagePullPolicy: IfNotPresent
          name: container-apollo-config-server-dev
          ports:
            - protocol: TCP
              containerPort: 8080
          volumeMounts:
            - name: volume-configmap-apollo-config-server-dev
              mountPath: /apollo-configservice/config
          
          env:
            - name: APOLLO_CONFIG_SERVICE_NAME
              value: "service-apollo-config-server-dev.dev"
      
              #readinessProbe:
              #tcpSocket:
              #port: 8080
              #initialDelaySeconds: 10     # 实际环境部署可以打开健康检测，根据需求调整时间
              #periodSeconds: 5
      
              #livenessProbe:
              #tcpSocket:
              #port: 8080
              #initialDelaySeconds:  120
              #periodSeconds: 10
      
      dnsPolicy: ClusterFirst
      restartPolicy: Always
```

- ##### service-yaml配置清单

```yaml
root@k8s-master01:/data/k8s-yaml/apollo-configservice-dev# cat svc.yaml 
---
kind: Service
apiVersion: v1
metadata:
  namespace: dev
  name: service-apollo-meta-server-dev
  labels:
    app: service-apollo-meta-server-dev
spec:
  ports:
    - protocol: TCP
      port: 8080
      targetPort: 8080
  selector:
    app: pod-apollo-config-server-dev
  type: ClusterIP
  clusterIP: None                            # statefulset使用的无头service的方式
  sessionAffinity: ClientIP

---
kind: Service
apiVersion: v1
metadata:
  namespace: dev
  name: service-apollo-config-server-dev	# Eureka的service配置,后面会用ingress做七层负载			
  labels:
    app: service-apollo-config-server-dev
spec:
  ports:
    - protocol: TCP
      port: 8080
      targetPort: 8080
  selector:
    app: pod-apollo-config-server-dev 
  sessionAffinity: ClientIP
```

- ##### 执行命令创建资源对象

```shell
root@k8s-master01:/data/k8s-yaml/apollo-configservice-dev# pwd
/data/k8s-yaml/apollo-configservice-dev
root@k8s-master01:/data/k8s-yaml/apollo-configservice-dev# kubectl apply -f cm.yaml
root@k8s-master01:/data/k8s-yaml/apollo-configservice-dev# kubectl apply -f svc.yaml
root@k8s-master01:/data/k8s-yaml/apollo-configservice-dev# kubectl apply -f ingress.yaml
root@k8s-master01:/data/k8s-yaml/apollo-configservice-dev# kubectl apply -f statefulset.yaml
```

#####  2.2：apollo-configservice-dev环境配置清单内容

- ##### configmap-yaml配置清单

```yaml
root@k8s-master01:/data/k8s-yaml/apollo-adminservice-dev# pwd
/data/k8s-yaml/apollo-adminservice-dev
root@k8s-master01:/data/k8s-yaml/apollo-adminservice-dev# cat cm.yaml 
apiVersion: v1
kind: ConfigMap
metadata:
  name: configmap-apollo-admin-server-dev
  namespace: dev
data:
  application-github.properties: |
    # DataSource
    spring.datasource.url = jdbc:mysql://service-mysql-for-apollo-dev-env:3306/DevApolloConfigDB?characterEncoding=utf8
    spring.datasource.username = apolloconfig
    spring.datasource.password = 123456
    eureka.service.url = http://statefulset-apollo-config-server-dev-0.service-apollo-meta-server-dev:8080/eureka/
  app.properties: |
    appId=100003172
```

- ##### deployment-yaml配置清单

```yaml
root@k8s-master01:/data/k8s-yaml/apollo-adminservice-dev# cat dp.yaml 
---
kind: Deployment
apiVersion: apps/v1
metadata:
  namespace: dev
  name: deployment-apollo-admin-server-dev
  labels:
    app: deployment-apollo-admin-server-dev
spec:
  replicas: 1
  selector:
    matchLabels:
      app: pod-apollo-admin-server-dev
  revisionHistoryLimit: 10
  progressDeadlineSeconds: 600
  strategy:
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 1
    type: RollingUpdate
  template:
    metadata:
      labels:
        app: pod-apollo-admin-server-dev
        name: pod-apollo-admin-server-dev
    spec:
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: app
                  operator: In
                  values:
                  - pod-apollo-admin-server-dev
              topologyKey: kubernetes.io/hostname
      
      volumes:
        - name: volume-configmap-apollo-admin-server-dev
          configMap:
            name: configmap-apollo-admin-server-dev
            #items:
            #  - key: application-github.properties
            #    path: application-github.properties
      
            #initContainers:
            #- image: docker pull alpine
            #name: check-service-apollo-config-server-dev
            #command: ['bash', '-c', "curl --connect-timeout 2 --max-time 5 --retry 60 --retry-delay 1 --retry-max-time 120 service-apollo-config-server-dev.sre:8080"] # 需调整
      
      containers:
        - image: harbor.od.com/infra/apollo-adminservice:v1.7.2
          securityContext:
            privileged: true
          imagePullPolicy: IfNotPresent
          name: container-apollo-admin-server-dev
          ports:
            - protocol: TCP
              containerPort: 8090
          
          volumeMounts:
            - name: volume-configmap-apollo-admin-server-dev
              mountPath: /apollo-adminservice/config
          
          env:
            - name: APOLLO_ADMIN_SERVICE_NAME
              value: "service-apollo-admin-server-dev.dev"
          
              #readinessProbe:
              #tcpSocket:
              #port: 8090
              #initialDelaySeconds: 10
              #periodSeconds: 5
          
              #livenessProbe:
              #tcpSocket:
              #port: 8090
              #initialDelaySeconds: 120
              #periodSeconds: 10
          
      dnsPolicy: ClusterFirst
      restartPolicy: Always
```

- ##### service-yaml配置清单

```yaml
root@k8s-master01:/data/k8s-yaml/apollo-adminservice-dev# cat svc.yaml 
---
kind: Service
apiVersion: v1
metadata:
  namespace: dev
  name: service-apollo-admin-server-dev
  labels:
    app: service-apollo-admin-server-dev
spec:
  ports:
    - protocol: TCP
      port: 8090
      targetPort: 8090
  selector:
    app: pod-apollo-admin-server-dev
  type: ClusterIP
  sessionAffinity: ClientIP

```

- ##### 执行命令创建资源对象

```shell
root@k8s-master01:/data/k8s-yaml/apollo-adminservice-dev# kubectl apply -f cm.yaml
root@k8s-master01:/data/k8s-yaml/apollo-adminservice-dev# kubectl apply -f svc.yaml
root@k8s-master01:/data/k8s-yaml/apollo-adminservice-dev# kubectl apply -f dp.yaml
```

- ##### 解析hosts,并验证confiservice和adminservice是否注册到Eureka

```http
http://dev.linux.com/
DS Replicas
statefulset-apollo-config-server-prod-0.service-apollo-meta-server-prod
APOLLO-ADMINSERVICE	n/a (1)	(1)	UP (1) - deployment-apollo-admin-server-prod-774478577f-52xrn:apollo-adminservice:8080
APOLLO-CONFIGSERVICE	n/a (1)	(1)	UP (1) - statefulset-apollo-config-server-prod-0.service-apollo-meta-server-prod.prod.svc.cluster.local:apollo-configservice:8080
```



##### 2.3：apollo-portal配置清单列表内容(数据库方面的操作,请参考Apollo交付至kubernetes文档)

- ##### Mysql-Endpoints配置清单

```yaml
root@k8s-master01:/data/k8s-yaml/apollo-portal-mysql-default# pwd
/data/k8s-yaml/apollo-portal-mysql-default
root@k8s-master01:/data/k8s-yaml/apollo-portal-mysql-default# cat service-mysql-for-apollo-portal-env.yaml 
#
# Copyright 2021 Apollo Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
---
# 为外部 mysql 服务设置 service
kind: Service
apiVersion: v1
metadata:
  name: service-mysql-for-portal-server
  labels:
    app: service-mysql-for-portal-server
spec:
  ports:
    - protocol: TCP
      port: 3306
      targetPort: 3306
  type: ClusterIP
  sessionAffinity: None

---
kind: Endpoints
apiVersion: v1
metadata:
  name: service-mysql-for-portal-server
subsets:
  - addresses:
      - ip: 172.18.18.20
    ports:
      - protocol: TCP
        port: 3306      
```

- ##### configmap-yaml配置清单

```yaml
root@k8s-master01:/data/k8s-yaml/apollo-portal# pwd
/data/k8s-yaml/apollo-portal
root@k8s-master01:/data/k8s-yaml/apollo-portal# cat cm.yaml 
apiVersion: v1
kind: ConfigMap
metadata:
  name: configmap-apollo-portal-server
data:
  application-github.properties: |
    # DataSource
    spring.datasource.url = jdbc:mysql://service-mysql-for-portal-server:3306/ApolloPortalDB?characterEncoding=utf8 
    spring.datasource.username = apolloportal
    spring.datasource.password = 123456
  app.properties: |
    appId=100003173
  apollo-env.properties: |
    #dev.meta=http://service-apollo-config-server-dev.dev:8080
    #fat.meta=http://service-apollo-config-server-test-alpha.test:8080
    uat.meta=http://service-apollo-config-server-test-beta.uat:8080
    pro.meta=http://service-apollo-config-server-prod.prod:8080
```

- ##### deployment-yaml配置清单

```yaml
root@k8s-master01:/data/k8s-yaml/apollo-portal# pwd
/data/k8s-yaml/apollo-portal
root@k8s-master01:/data/k8s-yaml/apollo-portal# cat cm.yaml 
apiVersion: v1
kind: ConfigMap
metadata:
  name: configmap-apollo-portal-server
data:
  application-github.properties: |
    # DataSource
    spring.datasource.url = jdbc:mysql://service-mysql-for-portal-server:3306/ApolloPortalDB?characterEncoding=utf8 
    spring.datasource.username = apolloportal
    spring.datasource.password = 123456
  app.properties: |
    appId=100003173
  apollo-env.properties: |
    #dev.meta=http://service-apollo-config-server-dev.dev:8080
    #fat.meta=http://service-apollo-config-server-test-alpha.test:8080
    uat.meta=http://service-apollo-config-server-test-beta.uat:8080
    pro.meta=http://service-apollo-config-server-prod.prod:8080
root@k8s-master01:/data/k8s-yaml/apollo-portal# ls
cm.yaml  dp-single.yaml  dp.yaml  ingress.yaml  svc.yaml
root@k8s-master01:/data/k8s-yaml/apollo-portal# cat dp.yaml 
---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: deployment-apollo-portal-server
  labels:
    app: deployment-apollo-portal-server
spec:
  # 1 个实例，实际生产环境根据情况来设置
  replicas: 1
  selector:
    matchLabels:
      app: pod-apollo-portal-server
  strategy:
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 1
    type: RollingUpdate
  template:
    metadata:
      labels:
        app: pod-apollo-portal-server
    spec:
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: app
                  operator: In
                  values:
                  - pod-apollo-portal-server
              topologyKey: kubernetes.io/hostname
      
      volumes:
        - name: volume-configmap-apollo-portal-server
          configMap:
            name: configmap-apollo-portal-server
            #items:
            #  - key: application-github.properties
            #    path: application-github.properties
            #  - key: apollo-env.properties
            #    path: apollo-env.properties
      
      #initContainers:
        # 确保 admin-service 正常提供服务
        #- image: alpine-bash:3.8
        #  name: check-service-apollo-admin-server-dev
        #  command: ['bash', '-c', "curl --connect-timeout 2 --max-time 5 --retry 60 --retry-delay 1 --retry-max-time 120 service-apollo-admin-server-dev.sre:8090"]
        #- image: alpine-bash:3.8
        #  name: check-service-apollo-admin-server-alpha
        #  command: ['bash', '-c', "curl --connect-timeout 2 --max-time 5 --retry 60 --retry-delay 1 --retry-max-time 120 service-apollo-admin-server-test-alpha.sre:8090"]
        #- image: alpine-bash:3.8
        #  name: check-service-apollo-admin-server-beta
        #  command: ['bash', '-c', "curl --connect-timeout 2 --max-time 5 --retry 60 --retry-delay 1 --retry-max-time 120 service-apollo-admin-server-test-beta.sre:8090"]
        #- image: alpine-bash:3.8
        #  name: check-service-apollo-admin-server-prod
        #  command: ['bash', '-c', "curl --connect-timeout 2 --max-time 5 --retry 60 --retry-delay 1 --retry-max-time 120 service-apollo-admin-server-prod.sre:8090"]    
      
      containers:
        - image: harbor.od.com/infra/apollo-portal:v1.7.2    # 更改为你的 docker registry 下的 image
          securityContext:
            privileged: true
          imagePullPolicy: IfNotPresent
          name: container-apollo-portal-server
          ports:
            - protocol: TCP
              containerPort: 8070
          
          volumeMounts:
            - name: volume-configmap-apollo-portal-server
              mountPath: /apollo-portal/config
            #  subPath: application-github.properties
            #- name: volume-configmap-apollo-portal-server
            #  mountPath: /apollo-portal-server/config/apollo-env.properties
            #  subPath: apollo-env.properties
          
          env:
            - name: APOLLO_PORTAL_SERVICE_NAME
              value: "service-apollo-portal-server"
          
          readinessProbe:
            tcpSocket:
              port: 8070
            initialDelaySeconds: 150                  # 根据服务器配置做调整,性能优时间设置短
            periodSeconds: 5
          
          livenessProbe:
            tcpSocket:
              port: 8070
            # 120s 内, server 未启动则重启 container
            initialDelaySeconds: 150
            periodSeconds: 15
          
      dnsPolicy: ClusterFirst
      restartPolicy: Always
```

- ##### ingress-yaml配置清单

```yaml
root@k8s-master01:/data/k8s-yaml/apollo-portal# cat ingress.yaml 
apiVersion: networking.k8s.io/v1beta1    
kind: Ingress
metadata: 
  annotations:
    nginx.ingress.kubernetes.io/affinity: "cookie"  # 解决会话保持 
    nginx.ingress.kubernetes.io/session-cookie-name: "route" 
    nginx.ingress.kubernetes.io/session-cookie-expires: "172800" 
    nginx.ingress.kubernetes.io/session-cookie-max-age: "172800" 
    kubernetes.io/ingress.class: "nginx"
  name: ingress-apollo-portal-server
spec:
  rules:
  - host: portal.linux.com
    http:
      paths:
      - path: /
        backend: 
          serviceName: service-apollo-portal-server
          servicePort: 8070
```

- ##### service-yaml配置清单

```yaml
root@k8s-master01:/data/k8s-yaml/apollo-portal# cat svc.yaml 
apiVersion: v1
kind: Service
metadata: 
  name: service-apollo-portal-server
  labels:
    app: service-apollo-portal-server        
  #namespace: infra
spec:
  ports:
  - protocol: TCP
    port: 8070
    targetPort: 8070
  selector: 
    app: pod-apollo-portal-server
  # portal session 保持
  sessionAffinity: ClientIP
```

- ##### 执行命令创建资源对象(第一次如果启动失败的话,可以手动删除portal关联的pod)

```shell
root@k8s-master01:/data/k8s-yaml/apollo-portal-mysql-default# kubectl apply -f service-mysql-for-apollo-portal-env.yaml
root@k8s-master01:/data/k8s-yaml/apollo-portal# kubectl apply -f cm.yaml
root@k8s-master01:/data/k8s-yaml/apollo-portal# kubectl apply -f ingress.yaml
root@k8s-master01:/data/k8s-yaml/apollo-portal# kubectl apply -f svc.yaml
```

- ##### hosts解析,并且通过web浏览器验证(如果是虚拟环境做实验,内存如不足的话,建议就做2个环境的测试)

```http
http://portal.linux.com/
创建项目
自行创建删除验证相关功能是否正常.

如果是用的2个环境做测试的,ApolloPortalDB数据库的ServerConfig的环境字段需要修改,否则pod的启动日志会出现
cannot find apollo.meta,因为数据库是配置了4个环境的Value，会定期做扫描的,根据需求自行设置即可!
```

