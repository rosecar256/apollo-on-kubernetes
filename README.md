apollo配置中心介绍

Apollo（阿波罗）是携程框架部门研发的分布式配置中心，能够集中化管理应用不同环境、不同集群的配置，配置修改后能够实时推送到应用端，并且具备规范的权限、流程治理等特性，适用于微服务配置管理场景。

本试验是参照一下文档实现最终流程

https://github.com/ctripcorp/apollo/tree/master/scripts/apollo-on-kubernetes

http://www.sunrisenan.com/docs/kubernetes/k8s05.html
