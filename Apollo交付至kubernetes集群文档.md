##### Apollo报错Eureka连接问题的参考地址：https://github.com/ctripcorp/apollo/issues/1138

##### 基础镜像地址：FROM sunrisenan/jre8:8u112



#### 一：在kubernetes集群里集成Apollo配置中心

##### 1：Apollo简介

```tex
Apollo（阿波罗）是携程框架部门研发的分布式配置中心，能够集中化管理应用不同环境、不同集群的配置，配置修改后能够实时推送到应用端，并且具备规范的权限、流程治理等特性，适用于微服务配置管理场景。
```

##### 1.1：Apollo地址

```tex
Apollo官方地址：https://github.com/ctripcorp/apollo

官方release包地址：https://github.com/ctripcorp/apollo/releases
```



#### 二：实战交付apollo配置中心组件–configservice到kubernetes集群

##### 2.1：根据自己的情况进行安装

```shell
注意：mysql的版本要5.6以上，mariadb要用10.1以上,Mariadb安装参考地址(https://www.shuzhiduo.com/A/amd0q9mqzg/)

root@k8s-master03:~# apt-get update -y
root@k8s-master03:~# apt-get install mysql-server mysql-client
The following NEW packages will be installed:
  libcgi-fast-perl libcgi-pm-perl libencode-locale-perl libevent-core-2.1-7 libevent-pthreads-2.1-7 libfcgi-perl libhtml-parser-perl libhtml-tagset-perl
  libhtml-template-perl libhttp-date-perl libhttp-message-perl libio-html-perl liblwp-mediatypes-perl libmecab2 libtimedate-perl liburi-perl mecab-ipadic
  mecab-ipadic-utf8 mecab-utils mysql-client mysql-client-8.0 mysql-client-core-8.0 mysql-server mysql-server-8.0 mysql-server-core-8.0

```

##### 2.1.1：查看/启动/停止 MySQL服务状态

```shell
systemctl status mysql
systemctl start mysql
systemctl stop mysql
```

##### 2.1.2：MySQL快速安装脚本`mysql_secure_installation`-安全设置之一，选择MySQL设置密码的强度

```shell
Securing the MySQL server deployment.

Connecting to MySQL using a blank password.

VALIDATE PASSWORD COMPONENT can be used to test passwords
and improve security. It checks the strength of password
and allows the users to set only those passwords which are
secure enough. Would you like to setup VALIDATE PASSWORD component?

Press y|Y for Yes, any other key for No: y

There are three levels of password validation policy:

LOW    Length >= 8
MEDIUM Length >= 8, numeric, mixed case, and special characters
STRONG Length >= 8, numeric, mixed case, special characters and dictionary                  file

Please enter 0 = LOW, 1 = MEDIUM and 2 = STRONG: 0
Please set the password for root here.
New password: 

Re-enter new password: # 输入密码

Estimated strength of the password: 50 
Do you wish to continue with the password provided?(Press y|Y for Yes, any other key for No) : y

By default, a MySQL installation has an anonymous user,
allowing anyone to log into MySQL without having to have
a user account created for them. This is intended only for
testing, and to make the installation go a bit smoother.
You should remove them before moving into a production
environment.

Remove anonymous users? (Press y|Y for Yes, any other key for No) :y
Success.


Normally, root should only be allowed to connect from
'localhost'. This ensures that someone cannot guess at
the root password from the network.

Disallow root login remotely? (Press y|Y for Yes, any other key for No) :No
 ... skipping.
By default, MySQL comes with a database named 'test' that
anyone can access. This is also intended only for testing,
and should be removed before moving into a production
environment.


Remove test database and access to it? (Press y|Y for Yes, any other key for No) :y
 - Dropping test database...
Success.

 - Removing privileges on test database...
Success.

Reloading the privilege tables will ensure that all changes
made so far will take effect immediately.

Reload privilege tables now? (Press y|Y for Yes, any other key for No) :y
Success.

All done!
```

##### 2.1.3：命令行登录mysql,并且简单查看

```mysql
root@k8s-master03:~# mysql -u root -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 11
Server version: 8.0.25-0ubuntu0.20.04.1 (Ubuntu)

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
4 rows in set (0.01 sec)

mysql> use mysql;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
mysql> select user,host from user;
+------------------+-----------+
| user             | host      |
+------------------+-----------+
| debian-sys-maint | localhost |
| mysql.infoschema | localhost |
| mysql.session    | localhost |
| mysql.sys        | localhost |
| root             | localhost |
+------------------+-----------+
5 rows in set (0.00 sec)

# mysql8.0修改root远程登录并且设置密码命令
ALTER USER ‘root’@’%’ IDENTIFIED WITH mysql_native_password BY’密码’;
flush privileges; //刷新用户权限
```

##### 2.1.4：修改mysql配置文件

```shell
root@k8s-master03:~# vi /etc/mysql/mysql.conf.d/mysqld.cnf
[mysqld]
character_set_server = utf8mb4
collation_server = utf8mb4_general_ci
init_connect = "SET NAMES 'utf8mb4'"


# If MySQL is running as a replication slave, this should be
# changed. Ref https://dev.mysql.com/doc/refman/8.0/en/server-system-variables.html#sysvar_tmpdir
# tmpdir                = /tmp
#
# Instead of skip-networking the default is now to listen only on
# localhost which is more compatible and is not less secure.
bind-address            = 0.0.0.0

root@k8s-master03:~# vi /etc/mysql/mysql.conf.d/mysql.cnf
[mysql]
default-character-set = utf8mb4


root@k8s-master03:~# systemctl restart mysql
```

##### 2.1.5：重新进入mysql并且验证相关配置是否生效

```mysql
root@k8s-master03:~# mysql -u root -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 8


mysql> \s
--------------
mysql  Ver 8.0.25-0ubuntu0.20.04.1 for Linux on x86_64 ((Ubuntu))

Connection id:		8
Current database:	
Current user:		root@localhost
SSL:			Not in use
Current pager:		stdout
Using outfile:		''
Using delimiter:	;
Server version:		8.0.25-0ubuntu0.20.04.1 (Ubuntu)
Protocol version:	10
Connection:		Localhost via UNIX socket
Server characterset:	utf8mb4
Db     characterset:	utf8mb4
Client characterset:	utf8mb4
Conn.  characterset:	utf8mb4
UNIX socket:		/var/run/mysqld/mysqld.sock
Binary data as:		Hexadecimal
Uptime:			1 min 37 sec

Threads: 2  Questions: 9  Slow queries: 0  Opens: 117  Flush tables: 3  Open tables: 36  Queries per second avg: 0.092
--------------
```

##### 2.1.6：执行创建对应的数据库SQL文件

```sql
https://github.com/ctripcorp/apollo/blob/1.7.2/scripts/sql/apolloconfigdb.sql

apolloconfigdb.sql   # 太多内容,此处省略。

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

# Create Database
# ------------------------------------------------------------
CREATE DATABASE IF NOT EXISTS ApolloConfigDB DEFAULT CHARACTER SET = utf8mb4;

Use ApolloConfigDB;

# Dump of table app
# ------------------------------------------------------------

DROP TABLE IF EXISTS `App`;

CREATE TABLE `App` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `AppId` varchar(500) NOT NULL DEFAULT 'default' COMMENT 'AppID',
  `Name` varchar(500) NOT NULL DEFAULT 'default' COMMENT '应用名',
  `OrgId` varchar(32) NOT NULL DEFAULT 'default' COMMENT '部门Id',
  `OrgName` varchar(64) NOT NULL DEFAULT 'default' COMMENT '部门名字',
  `OwnerName` varchar(500) NOT NULL DEFAULT 'default' COMMENT 'ownerName',
  `OwnerEmail` varchar(500) NOT NULL DEFAULT 'default' COMMENT 'ownerEmail',
  `IsDeleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '1: deleted, 0: normal',
  `DataChange_CreatedBy` varchar(32) NOT NULL DEFAULT 'default' COMMENT '创建人邮箱前缀',
  `DataChange_CreatedTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `DataChange_LastModifiedBy` varchar(32) DEFAULT '' COMMENT '最后修改人邮箱前缀',
  `DataChange_LastTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`Id`),
  KEY `AppId` (`AppId`(191)),
  KEY `DataChange_LastTime` (`DataChange_LastTime`),
  KEY `IX_Name` (`Name`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='应用表';



# Dump of table appnamespace
# ------------------------------------------------------------

DROP TABLE IF EXISTS `AppNamespace`;

CREATE TABLE `AppNamespace` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `Name` varchar(32) NOT NULL DEFAULT '' COMMENT 'namespace名字，注意，需要全局唯一',
  `AppId` varchar(32) NOT NULL DEFAULT '' COMMENT 'app id',
  `Format` varchar(32) NOT NULL DEFAULT 'properties' COMMENT 'namespace的format类型',
  `IsPublic` bit(1) NOT NULL DEFAULT b'0' COMMENT 'namespace是否为公共',
  `Comment` varchar(64) NOT NULL DEFAULT '' COMMENT '注释',
  `IsDeleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '1: deleted, 0: normal',
  `DataChange_CreatedBy` varchar(32) NOT NULL DEFAULT '' COMMENT '创建人邮箱前缀',
  `DataChange_CreatedTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `DataChange_LastModifiedBy` varchar(32) DEFAULT '' COMMENT '最后修改人邮箱前缀',
  `DataChange_LastTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY (`Id`),
  KEY `IX_AppId` (`AppId`),
  KEY `Name_AppId` (`Name`,`AppId`),
  KEY `DataChange_LastTime` (`DataChange_LastTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='应用namespace定义';

```

##### 2.1.7：下载并将SQL导入到数据库

```shell
root@k8s-master03:~# wget https://raw.githubusercontent.com/ctripcorp/apollo/1.7.2/scripts/sql/apolloconfigdb.sql -O apolloconfigdb.sql
--2021-07-06 20:02:58--  https://raw.githubusercontent.com/ctripcorp/apollo/1.7.2/scripts/sql/apolloconfigdb.sql

root@k8s-master03:~# mysql -uroot -p < apolloconfigdb.sql 
Enter password: 
root@k8s-master03:~#

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| ApolloConfigDB     |
| information_schema |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
5 rows in set (0.01 sec)

mysql> use ApolloConfigDB;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
mysql> show tables;
+--------------------------+
| Tables_in_ApolloConfigDB |
+--------------------------+
| AccessKey                |
| App                      |
| AppNamespace             |
| Audit                    |
| Cluster                  |
| Commit                   |
| GrayReleaseRule          |
| Instance                 |
| InstanceConfig           |
| Item                     |
| Namespace                |
| NamespaceLock            |
| Release                  |
| ReleaseHistory           |
| ReleaseMessage           |
| ServerConfig             |
+--------------------------+
16 rows in set (0.01 sec)
```

##### 2.1.8：数据库用户授权

```mysql
set global validate_password.policy=0;   # Your password does not satisfy the current policy requirements
set global validate_password.length=4;

#创建账户
CREATE USER 'apolloconfig'@'172.18.18.%' IDENTIFIED BY '123456';

#赋予权限，with grant option这个选项表示该用户可以将自己拥有的权限授权给别人
grant INSERT,DELETE,UPDATE,SELECT on ApolloConfigDB.* to 'apolloconfig'@'172.18.18.%';

#改密码&授权超用户，flush privileges 命令本质上的作用是将当前user和privilige表中的用户信息/权限设置从mysql库(MySQL数据库的内置库)中提取到内存里
flush privileges;

mysql> select user,host from mysql.user;
+------------------+-------------+
| user             | host        |
+------------------+-------------+
| apolloconfig     | 172.18.18.% |
| debian-sys-maint | localhost   |
| mysql.infoschema | localhost   |
| mysql.session    | localhost   |
| mysql.sys        | localhost   |
| root             | localhost   |
+------------------+-------------+
6 rows in set (0.00 sec)
```

##### 2.1.9：修改eureka的连接地址等配置

```mysql
mysql> select * from ServerConfig  \G
*************************** 1. row ***************************
                       Id: 1
                      Key: eureka.service.url
                  Cluster: default
                    Value: http://localhost:8080/eureka/
                  Comment: Eureka服务Url，多个service以英文逗号分隔
                IsDeleted: 0x00
     DataChange_CreatedBy: default
   DataChange_CreatedTime: 2021-07-06 20:03:53
DataChange_LastModifiedBy: 
      DataChange_LastTime: 2021-07-06 20:03:53
*************************** 2. row ***************************
                       Id: 2
                      Key: namespace.lock.switch
                  Cluster: default
                    Value: false
                  Comment: 一次发布只能有一个人修改开关
                IsDeleted: 0x00
     DataChange_CreatedBy: default
   DataChange_CreatedTime: 2021-07-06 20:03:53
DataChange_LastModifiedBy: 
      DataChange_LastTime: 2021-07-06 20:03:53
...........
5 rows in set (0.00 sec)

# 修改eureka地址命令：
update ApolloConfigDB.ServerConfig set ServerConfig.Value="http://172.18.18.188:30002/eureka" where ServerConfig.Key="eureka.service.url";

# 再次查看
mysql> select * from ServerConfig  \G
*************************** 1. row ***************************
                       Id: 1
                      Key: eureka.service.url
                  Cluster: default
                    Value: http://172.18.18.188:30002/eureka
                  Comment: Eureka服务Url，多个service以英文逗号分隔
                IsDeleted: 0x00
     DataChange_CreatedBy: default
   DataChange_CreatedTime: 2021-07-06 20:03:53
DataChange_LastModifiedBy: 
      DataChange_LastTime: 2021-07-06 20:49:12

```

##### 2.2.1：添加hosts主机解析

```shell
# 所有主机都要添加下面的解析
root@a6c1b2fb8a44:/usr/local/tomcat/conf# cat /etc/hosts
127.0.0.1	localhost
::1	localhost ip6-localhost ip6-loopback
fe00::0	ip6-localnet
ff00::0	ip6-mcastprefix
ff02::1	ip6-allnodes
ff02::2	ip6-allrouters
172.17.0.2	a6c1b2fb8a44

172.18.18.18 config.od.com

```

##### 2.2.2：准备软件包并解压

```shell
https://github.com/ctripcorp/apollo/releases/download/v1.7.2/apollo-configservice-1.7.2-github.zip
root@k8s-master01:/usr/local/src#  wget https://github.com/ctripcorp/apollo/releases/download/v1.7.2/apollo-configservice-1.7.2-github.zip
HTTP request sent, awaiting response... 200 OK

root@k8s-master01:/usr/local/src# mkdir -p /data/dockerfile/apollo-configservice
root@k8s-master01:/usr/local/src# unzip -o apollo-configservice-1.7.2-github.zip -d /data/dockerfile/apollo-configservice
Archive:  apollo-configservice-1.7.2-github.zip


root@k8s-master01:/usr/local/src# cd /data/dockerfile/apollo-configservice
root@k8s-master01:/data/dockerfile/apollo-configservice# ls
apollo-configservice-1.7.2-sources.jar	apollo-configservice-1.7.2.jar	apollo-configservice.conf  config  scripts
root@k8s-master01:/data/dockerfile/apollo-configservice# ls -utlh
total 62M
drwxr-xr-x 2 root root  65 Jul  7 02:19 config
drwxr-xr-x 2 root root  43 Jul  7 02:19 scripts
-rwxr-xr-x 1 root root 62M Jan 30 20:33 apollo-configservice-1.7.2.jar
-rwxr-xr-x 1 root root 47K Jan 30 20:33 apollo-configservice-1.7.2-sources.jar
-rw-r--r-- 1 root root  57 Jan 30 20:29 apollo-configservice.conf

root@k8s-master01:/data/dockerfile/apollo-configservice# rm -rf apollo-configservice-1.7.2-sources.jar
```

##### 2.2.3：增加mysql的hosts解析,并且修改配置文件的数据库连接(根据实际情况使用域名解析,实际k8s部署环境当中，此域名连接mysql的方式需要解析hosts到pod的容器中)

```shell
root@k8s-master01:/data/dockerfile/apollo-configservice# cat /etc/hosts
127.0.0.1 localhost
127.0.1.1 dustin

# The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters

172.18.18.18 config.od.com
172.18.18.19 harbor.od.com
172.18.18.20 mysql.od.com


root@k8s-master01:/data/dockerfile/apollo-configservice/config# pwd
/data/dockerfile/apollo-configservice/config
root@k8s-master01:/data/dockerfile/apollo-configservice/config# cat application-github.properties 
# DataSource
spring.datasource.url = jdbc:mysql://mysql.od.com:3306/ApolloConfigDB?characterEncoding=utf8
spring.datasource.username = apolloconfig
spring.datasource.password = 123456
```

##### 2.2.4：拷贝并简单修改下启动脚本，删除停止的脚本

```shell
# 官网脚本地址
https://github.com/ctripcorp/apollo/blob/master/scripts/apollo-on-kubernetes/apollo-config-server/scripts/startup-kubernetes.sh

# 删除停止脚本
root@k8s-master01:/data/dockerfile/apollo-configservice/scripts# pwd
/data/dockerfile/apollo-configservice/scripts
root@k8s-master01:/data/dockerfile/apollo-configservice/scripts# ls
shutdown.sh  startup.sh
root@k8s-master01:/data/dockerfile/apollo-configservice/scripts# rm -rf shutdown.sh

# 修改启动脚本
root@k8s-master01:/data/dockerfile/apollo-configservice/scripts# cat startup.sh 
#!/bin/bash
#
# Copyright 2021 Apollo Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
SERVICE_NAME=apollo-configservice
## Adjust log dir if necessary
LOG_DIR=/opt/logs/apollo-config-server
## Adjust server port if necessary
SERVER_PORT=8080
APOLLO_CONFIG_SERVICE_NAME=$(hostname -i)
SERVER_URL="http://${APOLLO_CONFIG_SERVICE_NAME}:${SERVER_PORT}"

## Adjust memory settings if necessary
export JAVA_OPTS="-Xms128m -Xmx128m -Xss256k -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=384m -XX:NewSize=256m -XX:MaxNewSize=256m -XX:SurvivorRatio=8"

## Only uncomment the following when you are using server jvm
#export JAVA_OPTS="$JAVA_OPTS -server -XX:-ReduceInitialCardMarks"

########### The following is the same for configservice, adminservice, portal ###########
export JAVA_OPTS="$JAVA_OPTS -XX:ParallelGCThreads=4 -XX:MaxTenuringThreshold=9 -XX:+DisableExplicitGC -XX:+ScavengeBeforeFullGC -XX:SoftRefLRUPolicyMSPerMB=0 -XX:+ExplicitGCInvokesConcurrent -XX:+HeapDumpOnOutOfMemoryError -XX:-OmitStackTraceInFastThrow -Duser.timezone=Asia/Shanghai -Dclient.encoding.override=UTF-8 -Dfile.encoding=UTF-8 -Djava.security.egd=file:/dev/./urandom"
export JAVA_OPTS="$JAVA_OPTS -Dserver.port=$SERVER_PORT -Dlogging.file.name=$LOG_DIR/$SERVICE_NAME.log -XX:HeapDumpPath=$LOG_DIR/HeapDumpOnOutOfMemoryError/"

# Find Java
if [[ -n "$JAVA_HOME" ]] && [[ -x "$JAVA_HOME/bin/java" ]]; then
    javaexe="$JAVA_HOME/bin/java"
elif type -p java > /dev/null 2>&1; then
    javaexe=$(type -p java)
elif [[ -x "/usr/bin/java" ]];  then
    javaexe="/usr/bin/java"
else
    echo "Unable to find Java"
    exit 1
fi

if [[ "$javaexe" ]]; then
    version=$("$javaexe" -version 2>&1 | awk -F '"' '/version/ {print $2}')
    version=$(echo "$version" | awk -F. '{printf("%03d%03d",$1,$2);}')
    # now version is of format 009003 (9.3.x)
    if [ $version -ge 011000 ]; then
        JAVA_OPTS="$JAVA_OPTS -Xlog:gc*:$LOG_DIR/gc.log:time,level,tags -Xlog:safepoint -Xlog:gc+heap=trace"
    elif [ $version -ge 010000 ]; then
        JAVA_OPTS="$JAVA_OPTS -Xlog:gc*:$LOG_DIR/gc.log:time,level,tags -Xlog:safepoint -Xlog:gc+heap=trace"
    elif [ $version -ge 009000 ]; then
        JAVA_OPTS="$JAVA_OPTS -Xlog:gc*:$LOG_DIR/gc.log:time,level,tags -Xlog:safepoint -Xlog:gc+heap=trace"
    else
        JAVA_OPTS="$JAVA_OPTS -XX:+UseParNewGC"
        JAVA_OPTS="$JAVA_OPTS -Xloggc:$LOG_DIR/gc.log -XX:+PrintGCDetails"
        JAVA_OPTS="$JAVA_OPTS -XX:+UseConcMarkSweepGC -XX:+UseCMSCompactAtFullCollection -XX:+UseCMSInitiatingOccupancyOnly -XX:CMSInitiatingOccupancyFraction=60 -XX:+CMSClassUnloadingEnabled -XX:+CMSParallelRemarkEnabled -XX:CMSFullGCsBeforeCompaction=9 -XX:+CMSClassUnloadingEnabled  -XX:+PrintGCDateStamps -XX:+PrintGCApplicationConcurrentTime -XX:+PrintHeapAtGC -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=5 -XX:GCLogFileSize=5M"
    fi
fi

printf "$(date) ==== Starting ==== \n"

cd `dirname $0`/..
chmod 755 $SERVICE_NAME".jar"
./$SERVICE_NAME".jar" start

rc=$?;

if [[ $rc != 0 ]];
then
    echo "$(date) Failed to start $SERVICE_NAME.jar, return code: $rc"
    exit $rc;
fi

tail -f /dev/null


# 添加执行权限
root@k8s-master01:/data/dockerfile/apollo-configservice/scripts# chmod a+x startup.sh
```

##### 2.2.5：准备基础镜像,并且准备Dockerfile

```shell
# 官网github参考Dockerfile的地址
https://github.com/ctripcorp/apollo/blob/master/scripts/apollo-on-kubernetes/apollo-config-server/Dockerfile

# harbor镜像仓库,需要提前准备好
docker pull stanleyws/jre8:8u112
docker tag fa3a085d6ef1 harbor.od.com/base/jre8:8u112

root@k8s-master01:/data/k8s-ansible/pki# docker push harbor.od.com/base/jre8:8u112
The push refers to repository [harbor.od.com/base/jre8]
0690f10a63a5: Pushed 
c843b2cf4e12: Pushed 
fddd8887b725: Pushed 
42052a19230c: Pushed 
8d4d1ab5ff74: Pushed 
8u112: digest: sha256:733087bae1f15d492307fca1f668b3a5747045aad6af06821e3f64755268ed8e size: 1367


root@k8s-master01:/data/dockerfile/apollo-configservice# cat Dockerfile

FROM harbor.od.com/base/jre8:8u112

ENV VERSION 1.7.2

RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime &&\
    echo "Asia/Shanghai" > /etc/timezone

ADD apollo-configservice-${VERSION}.jar /apollo-configservice/apollo-configservice.jar
ADD config/ /apollo-configservice/config
ADD scripts/ /apollo-configservice/scripts

CMD ["/apollo-configservice/scripts/startup.sh"]
```

##### 2.2.6：制作docker镜像，并且上传到harbor

```shell
root@k8s-master01:/data/dockerfile/apollo-configservice# docker build . -t harbor.od.com/infra/apollo-configservice:v1.7.2
root@k8s-master01:/data/dockerfile/apollo-configservice# docker push harbor.od.com/infra/apollo-configservice:v1.7.2
```

##### 2.3.1：准备资源配置清单-ConfigMap

```yaml
# 参考官方文档
https://github.com/ctripcorp/apollo/blob/master/scripts/apollo-on-kubernetes/kubernetes/apollo-env-dev/service-apollo-config-server-dev.yaml

mkdir /data/k8s-yaml/apollo-configservice && cd /data/k8s-yaml/apollo-configservice
root@k8s-master01:/data/k8s-yaml/apollo-configservice# cat cm.yaml 
apiVersion: v1
kind: ConfigMap
metadata:
  name: apollo-configservice-cm
  namespace: infra
data:
  application-github.properties: |
    # DataSource
    #spring.datasource.url = jdbc:mysql://mysql.od.com:3306/ApolloConfigDB?characterEncoding=utf8
    spring.datasource.url = jdbc:mysql://172.18.18.20:3306/ApolloConfigDB?characterEncoding=utf8   # 直接写mysql数据库所在的服务器的地址
    spring.datasource.username = apolloconfig      # 也可以换成secret的方式进行部署
    spring.datasource.password = 123456
    eureka.service.url = http://172.18.18.188:30002/eureka      # 先用nodeport的方式注册到eureka,此处用的是k8s集群的vip地址。
  app.properties: |
    appId=100003171
```

##### 2.3.2：准备资源配置清单-deployment

 ```yaml
 root@k8s-master01:/data/k8s-yaml/apollo-configservice# cat dp.yaml 
 apiVersion: apps/v1
 kind: Deployment
 metadata:
   name: apollo-configservice
   namespace: infra
   labels: 
     name: apollo-configservice
 spec:
   replicas: 1
   strategy:
     type: RollingUpdate
     rollingUpdate: 
       maxUnavailable: 1
       maxSurge: 1
   revisionHistoryLimit: 10
   progressDeadlineSeconds: 600
   selector:
     matchLabels: 
       name: apollo-configservice
   template:
     metadata:
       labels: 
         app: apollo-configservice 
         name: apollo-configservice
     spec:
       restartPolicy: Always
       terminationGracePeriodSeconds: 60
       containers:        
         - name: apollo-configservice
           image: harbor.od.com/infra/apollo-configservice:v1.7.2
           imagePullPolicy: IfNotPresent
           #terminationMessagePath: /dev/termination-log
           #terminationMessagePolicy: File
           #securityContext: 
           #  runAsUser: 0
           ports:
             - containerPort: 8080
               protocol: TCP
           volumeMounts:
             - name: configmap-volume
               mountPath: /apollo-configservice/config
                 
           readinessProbe:
             tcpSocket:
               port: 8080
             initialDelaySeconds: 10
             periodSeconds: 5
 
           livenessProbe:
             tcpSocket:
               port: 8080
             initialDelaySeconds:  120
             periodSeconds: 10
 
       volumes:
         - name: configmap-volume
           configMap:
             name: apollo-configservice-cm
 ```

##### 2.3.3：准备资源配置清单-service

 ```yaml
 root@k8s-master01:/data/k8s-yaml/apollo-configservice# cat svc.yaml 
 apiVersion: v1
 kind: Service
 metadata: 
   name: apollo-configservice
   namespace: infra
 spec:
   ports:
   - protocol: TCP
     port: 8080
     targetPort: 8080
     nodePort: 30002            # 固定nodeport的地址
   selector: 
     app: apollo-configservice
   type: NodePort
 ```

##### 2.3.4：命令行执行部署操作

```shell
root@k8s-master01:/data/k8s-yaml/apollo-configservice# kubectl apply -f cm.yaml
root@k8s-master01:/data/k8s-yaml/apollo-configservice# kubectl apply -f dp.yaml
root@k8s-master01:/data/k8s-yaml/apollo-configservice# kubectl apply -f svc.yaml
```

##### 2.3.5：命令行验证是否正常启动,并且查看configservice是否正常有部署

```shell
root@k8s-master01:/data/k8s-yaml/apollo-configservice# kubectl logs -f -n infra apollo-configservice-75bc55876d-p6vkm
021-07-07 22:56:13.226  INFO 40 --- [trap-executor-0] c.n.d.s.r.aws.ConfigClusterResolver      : Resolving eureka endpoints via configuration
2021-07-07 22:56:18.767  INFO 40 --- [a-EvictionTimer] c.n.e.registry.AbstractInstanceRegistry  : Running the evict task with compensationTime 0ms
2021-07-07 22:57:13.227  INFO 40 --- [trap-executor-0] c.n.d.s.r.aws.ConfigClusterResolver      : Resolving eureka endpoints via configuration
2021-07-07 22:57:18.768  INFO 40 --- [a-EvictionTimer] c.n.e.registry.AbstractInstanceRegistry  : Running the evict task with compensationTime 0ms
2021-07-07 22:58:13.228  INFO 40 --- [trap-executor-0] c.n.d.s.r.aws.ConfigClusterResolver      : Resolving eureka endpoints via configuration
2021-07-07 22:58:18.769  INFO 40 --- [a-EvictionTimer] c.n.e.registry.AbstractInstanceRegistry  : Running the evict task with compensationTime 0ms

http://172.18.18.188:30002/

Application				AMIs				 Availability Zones	Status
APOLLO-CONFIGSERVICE	n/a (1)	(1)	UP (1) - apollo-configservice-75bc55876d-p6vkm:apollo-configservice:8080
```

##### 2.3.6：数据库使用域名的方式,需要添加hosts解析

```yaml
root@k8s-master01:/data/k8s-yaml/apollo-configservice# cat dp.yaml 
apiVersion: apps/v1
kind: Deployment
metadata:
  name: apollo-configservice
  namespace: infra
  labels: 
    name: apollo-configservice
spec:
  replicas: 1
  strategy:
    type: RollingUpdate
    rollingUpdate: 
      maxUnavailable: 1
      maxSurge: 1
  revisionHistoryLimit: 10
  progressDeadlineSeconds: 600
  selector:
    matchLabels: 
      name: apollo-configservice
  template:
    metadata:
      labels: 
        app: apollo-configservice 
        name: apollo-configservice
    spec:
      restartPolicy: Always
      terminationGracePeriodSeconds: 60
      hostAliases:
      - ip: "172.18.18.20"
        hostnames:
        - "mysql.od.com"    # 需要加此处的pod解析
      containers:        
        - name: apollo-configservice
          image: harbor.od.com/infra/apollo-configservice:v1.7.2
          imagePullPolicy: IfNotPresent
          #terminationMessagePath: /dev/termination-log
          #terminationMessagePolicy: File
          #securityContext: 
          #  runAsUser: 0
          ports:
            - containerPort: 8080
              protocol: TCP
          volumeMounts:
            - name: configmap-volume
              mountPath: /apollo-configservice/config
                
          readinessProbe:
            tcpSocket:
              port: 8080
            initialDelaySeconds: 10
            periodSeconds: 5

          livenessProbe:
            tcpSocket:
              port: 8080
            initialDelaySeconds:  120
            periodSeconds: 10

      volumes:
        - name: configmap-volume
          configMap:
            name: apollo-configservice-cm

# configmap的配置文件
root@k8s-master01:/data/k8s-yaml/apollo-configservice# cat cm.yaml 
apiVersion: v1
kind: ConfigMap
metadata:
  name: apollo-configservice-cm
  namespace: infra
data:
  application-github.properties: |
    # DataSource
    spring.datasource.url = jdbc:mysql://mysql.od.com:3306/ApolloConfigDB?characterEncoding=utf8
    spring.datasource.username = apolloconfig
    spring.datasource.password = 123456
    eureka.service.url = http://172.18.18.188:30002/eureka
  app.properties: |
    appId=100003171


# 此时pod当中就会添加mysql服务器的hosts解析
root@apollo-configservice-7db5886b8-d4bsm:/apollo-configservice/config# cat /etc/hosts 
# Kubernetes-managed hosts file.
127.0.0.1	localhost
::1	localhost ip6-localhost ip6-loopback
fe00::0	ip6-localnet
fe00::0	ip6-mcastprefix
fe00::1	ip6-allnodes
fe00::2	ip6-allrouters
10.244.32.140	apollo-configservice-7db5886b8-d4bsm

# Entries added by HostAliases.
172.18.18.20	mysql.od.com
```

##### 2.3.7：查看数据库的连接情况,都是通过node节点客户端ip连接到数据库的

```mysql
mysql> show processlist;
+----+-----------------+--------------------+----------------+---------+------+------------------------+------------------+
| Id | User            | Host               | db             | Command | Time | State                  | Info             |
+----+-----------------+--------------------+----------------+---------+------+------------------------+------------------+
|  5 | event_scheduler | localhost          | NULL           | Daemon  | 2055 | Waiting on empty queue | NULL             |
| 18 | root            | localhost          | NULL           | Query   |    0 | init                   | show processlist |
| 19 | apolloconfig    | 172.18.18.18:23972 | ApolloConfigDB | Sleep   |   17 |                        | NULL             |
| 20 | apolloconfig    | 172.18.18.18:22436 | ApolloConfigDB | Sleep   |    0 |                        | NULL             |
| 21 | apolloconfig    | 172.18.18.18:30756 | ApolloConfigDB | Sleep   |    0 |                        | NULL             |
| 22 | apolloconfig    | 172.18.18.18:47069 | ApolloConfigDB | Sleep   |    0 |                        | NULL             |
| 23 | apolloconfig    | 172.18.18.18:60099 | ApolloConfigDB | Sleep   |    0 |                        | NULL             |
| 24 | apolloconfig    | 172.18.18.18:11497 | ApolloConfigDB | Sleep   |  695 |                        | NULL             |
| 25 | apolloconfig    | 172.18.18.18:6195  | ApolloConfigDB | Sleep   |  695 |                        | NULL             |
| 26 | apolloconfig    | 172.18.18.18:60412 | ApolloConfigDB | Sleep   |  695 |                        | NULL             |
| 27 | apolloconfig    | 172.18.18.18:65320 | ApolloConfigDB | Sleep   |  695 |                        | NULL             |
| 28 | apolloconfig    | 172.18.18.18:40481 | ApolloConfigDB | Sleep   |  695 |                        | NULL             |
+----+-----------------+--------------------+----------------+---------+------+------------------------+------------------+
12 rows in set (0.01 sec)

# 跑在master01节点的宿主机上,所以对应的宿主机ip就是172.18.18.18
root@k8s-master01:/data/k8s-yaml/apollo-configservice# kubectl get po --all-namespaces -owide
NAMESPACE     NAME                                       READY   STATUS    RESTARTS   AGE   IP              NODE           NOMINATED NODE   READINESS GATES
infra         apollo-configservice-7db5886b8-d4bsm       1/1     Running   0          13m   10.244.32.140   k8s-master01   <none>           <none>
```

#### 三：实战交付apollo配置中心组件–adminservice到kubernetes集群

##### 3.1.1：准备软件包

 ```shell
 root@k8s-master01:/usr/local/src# wget -O /usr/local/src/apollo-adminservice-1.7.2-github.zip  https://github.com/ctripcorp/apollo/releases/download/v1.7.2/apollo-adminservice-1.7.2-github.zip
 mkdir /data/dockerfile/apollo-adminservice && unzip -o /usr/local/src/apollo-adminservice-1.7.2-github.zip -d /data/dockerfile/apollo-adminservice
 ```

##### 3.1.2：删除无用文件

```shell
root@k8s-master01:~#  rm -f /data/dockerfile/apollo-adminservice/apollo-adminservice-1.7.2-sources.jar 
root@k8s-master01:~# rm -f  /data/dockerfile/apollo-adminservice/apollo-adminservice.conf
root@k8s-master01:~# rm -f /data/dockerfile/apollo-adminservice/scripts/shutdown.sh
root@k8s-master01:~# cat /data/dockerfile/apollo-adminservice/config/app.properties
appId=100003172
jdkVersion=1.8
```

##### 3.2.1：制作Docker镜像

```shell
root@k8s-master01:~# cat /data/dockerfile/apollo-adminservice/config/application-github.properties
# DataSource
spring.datasource.url = jdbc:mysql://mysql.od.com:3306/ApolloConfigDB?characterEncoding=utf8
spring.datasource.username = apolloconfig
spring.datasource.password = 123456
```

##### 3.2.2：修改启动脚本

```shell
root@k8s-master01:~# cat /data/dockerfile/apollo-adminservice/scripts/startup.sh
#!/bin/bash
#
# Copyright 2021 Apollo Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
SERVICE_NAME=apollo-adminservice
## Adjust log dir if necessary
LOG_DIR=/opt/logs/apollo-admin-server
## Adjust server port if necessary
SERVER_PORT=8080
APOLLO_ADMIN_SERVICE_NAME=$(hostname -i)

# SERVER_URL="http://localhost:${SERVER_PORT}"
SERVER_URL="http://${APOLLO_ADMIN_SERVICE_NAME}:${SERVER_PORT}"

## Adjust memory settings if necessary
#export JAVA_OPTS="-Xms2560m -Xmx2560m -Xss256k -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=384m -XX:NewSize=1536m -XX:MaxNewSize=1536m -XX:SurvivorRatio=8"

## Only uncomment the following when you are using server jvm
#export JAVA_OPTS="$JAVA_OPTS -server -XX:-ReduceInitialCardMarks"

########### The following is the same for configservice, adminservice, portal ###########
export JAVA_OPTS="$JAVA_OPTS -XX:ParallelGCThreads=4 -XX:MaxTenuringThreshold=9 -XX:+DisableExplicitGC -XX:+ScavengeBeforeFullGC -XX:SoftRefLRUPolicyMSPerMB=0 -XX:+ExplicitGCInvokesConcurrent -XX:+HeapDumpOnOutOfMemoryError -XX:-OmitStackTraceInFastThrow -Duser.timezone=Asia/Shanghai -Dclient.encoding.override=UTF-8 -Dfile.encoding=UTF-8 -Djava.security.egd=file:/dev/./urandom"
export JAVA_OPTS="$JAVA_OPTS -Dserver.port=$SERVER_PORT -Dlogging.file.name=$LOG_DIR/$SERVICE_NAME.log -XX:HeapDumpPath=$LOG_DIR/HeapDumpOnOutOfMemoryError/"

# Find Java
if [[ -n "$JAVA_HOME" ]] && [[ -x "$JAVA_HOME/bin/java" ]]; then
    javaexe="$JAVA_HOME/bin/java"
elif type -p java > /dev/null 2>&1; then
    javaexe=$(type -p java)
elif [[ -x "/usr/bin/java" ]];  then
    javaexe="/usr/bin/java"
else
    echo "Unable to find Java"
    exit 1
fi

if [[ "$javaexe" ]]; then
    version=$("$javaexe" -version 2>&1 | awk -F '"' '/version/ {print $2}')
    version=$(echo "$version" | awk -F. '{printf("%03d%03d",$1,$2);}')
    # now version is of format 009003 (9.3.x)
    if [ $version -ge 011000 ]; then
        JAVA_OPTS="$JAVA_OPTS -Xlog:gc*:$LOG_DIR/gc.log:time,level,tags -Xlog:safepoint -Xlog:gc+heap=trace"
    elif [ $version -ge 010000 ]; then
        JAVA_OPTS="$JAVA_OPTS -Xlog:gc*:$LOG_DIR/gc.log:time,level,tags -Xlog:safepoint -Xlog:gc+heap=trace"
    elif [ $version -ge 009000 ]; then
        JAVA_OPTS="$JAVA_OPTS -Xlog:gc*:$LOG_DIR/gc.log:time,level,tags -Xlog:safepoint -Xlog:gc+heap=trace"
    else
        JAVA_OPTS="$JAVA_OPTS -XX:+UseParNewGC"
        JAVA_OPTS="$JAVA_OPTS -Xloggc:$LOG_DIR/gc.log -XX:+PrintGCDetails"
        JAVA_OPTS="$JAVA_OPTS -XX:+UseConcMarkSweepGC -XX:+UseCMSCompactAtFullCollection -XX:+UseCMSInitiatingOccupancyOnly -XX:CMSInitiatingOccupancyFraction=60 -XX:+CMSClassUnloadingEnabled -XX:+CMSParallelRemarkEnabled -XX:CMSFullGCsBeforeCompaction=9 -XX:+CMSClassUnloadingEnabled  -XX:+PrintGCDateStamps -XX:+PrintGCApplicationConcurrentTime -XX:+PrintHeapAtGC -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=5 -XX:GCLogFileSize=5M"
    fi
fi

printf "$(date) ==== Starting ==== \n"

cd `dirname $0`/..
chmod 755 $SERVICE_NAME".jar"
./$SERVICE_NAME".jar" start

rc=$?;

if [[ $rc != 0 ]];
then
    echo "$(date) Failed to start $SERVICE_NAME.jar, return code: $rc"
    exit $rc;
fi

tail -f /dev/null
```

- 在官网的基础上修改了两个参数

```tex
SERVER_PORT=8080
APOLLO_ADMIN_SERVICE_NAME=$(hostname -i)
```

##### 3.2.3：添加脚本执行权限,并且构建Dockerfile

```shell
root@k8s-master01:~# chmod a+x /data/dockerfile/apollo-adminservice/scripts/startup.sh

root@k8s-master01:/data/dockerfile/apollo-adminservice# cat Dockerfile
FROM harbor.od.com/base/jre8:8u112

ENV VERSION 1.7.2

RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime &&\
    echo "Asia/Shanghai" > /etc/timezone

ADD apollo-adminservice-${VERSION}.jar /apollo-adminservice/apollo-adminservice.jar
ADD config/ /apollo-adminservice/config
ADD scripts/ /apollo-adminservice/scripts

CMD ["/apollo-adminservice/scripts/startup.sh"]
```

- 制作镜像并推送

```shell
root@k8s-master01:/data/dockerfile/apollo-adminservice# docker build . -t harbor.od.com/infra/apollo-adminservice:v1.7.2
root@k8s-master01:/data/dockerfile/apollo-adminservice# docker push harbor.od.com/infra/apollo-adminservice:v1.7.2
```

##### 3.3.1：准备配置清单-ConfigMap

```yaml
root@k8s-master01:/data/dockerfile/apollo-configservice# mkdir /data/k8s-yaml/apollo-adminservice && cd /data/k8s-yaml/apollo-adminservice

root@k8s-master01:/data/k8s-yaml/apollo-adminservice# vi cm.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: apollo-adminservice-cm
  namespace: infra
data:
  application-github.properties: |
    # DataSource
    spring.datasource.url = jdbc:mysql://mysql.od.com:3306/ApolloConfigDB?characterEncoding=utf8
    spring.datasource.username = apolloconfig
    spring.datasource.password = 123456
    eureka.service.url = http://172.18.18.188:30002/eureka
  app.properties: |
    appId=100003172
```

##### 3.3.2：准备配置清单-Deployment

```yaml
root@k8s-master01:/data/k8s-yaml/apollo-adminservice# cat dp.yaml 
apiVersion: apps/v1
kind: Deployment
metadata:
  name: apollo-adminservice
  namespace: infra
  labels: 
    name: apollo-adminservice
spec:
  replicas: 1
  strategy:
    type: RollingUpdate
    rollingUpdate: 
      maxUnavailable: 1
      maxSurge: 1
  revisionHistoryLimit: 10
  progressDeadlineSeconds: 600
  selector:
    matchLabels: 
      name: apollo-adminservice
  template:
    metadata:
      labels: 
        app: apollo-adminservice 
        name: apollo-adminservice
    spec:
      restartPolicy: Always
      terminationGracePeriodSeconds: 60
      hostAliases:
      - ip: "172.18.18.20"
        hostnames:
        - "mysql.od.com"
      containers:        
        - name: apollo-adminservice
          image: harbor.od.com/infra/apollo-adminservice:v1.7.2
          imagePullPolicy: IfNotPresent
          terminationMessagePath: /dev/termination-log
          terminationMessagePolicy: File
          securityContext: 
            runAsUser: 0
          ports:
            - containerPort: 8080
              protocol: TCP
          volumeMounts:
            - name: configmap-volume
              mountPath: /apollo-adminservice/config
                
          readinessProbe:
            tcpSocket:
              port: 8080
            initialDelaySeconds: 10
            periodSeconds: 5

          livenessProbe:
            tcpSocket:
              port: 8080
            initialDelaySeconds:  120
            periodSeconds: 10

      volumes:
        - name: configmap-volume
          configMap:
            name: apollo-adminservice-cm
```

##### 3.3.3：声明式方式使用配置清单创建资源对象

```shell
root@k8s-master01:/data/k8s-yaml/apollo-adminservice# kubectl apply -f cm.yaml
root@k8s-master01:/data/k8s-yaml/apollo-adminservice# kubectl apply -f dp.yaml
```

##### 3.3.4：最后浏览器查看是否成功注册到Eureka

```http
http://172.18.18.188:30002/

Application	AMIs		Availability Zones	Status
APOLLO-ADMINSERVICE	    n/a (1)	(1)	        UP (1) - apollo-adminservice-97fb69b47-d9lkr:apollo-adminservice:8080
APOLLO-CONFIGSERVICE	n/a (1)	(1)	        UP (1) - apollo-configservice-7db5886b8-d4bsm:apollo-configservice:8080
```

#### 四：实战交付apollo配置中心组件–portal到kubernetes集群

##### 4.1.1：准备软件包

https://github.com/ctripcorp/apollo/releases

```shell
root@k8s-master01:/data/k8s-yaml/apollo-adminservice# wget -O /usr/local/src/apollo-portal-1.7.2-github.zip https://github.com/ctripcorp/apollo/releases/download/v1.7.2/apollo-portal-1.7.2-github.zip
root@k8s-master01:/data/k8s-yaml/apollo-adminservice# mkdir /data/dockerfile/apollo-portal && unzip -o /usr/local/src/apollo-portal-1.7.2-github.zip -d /data/dockerfile/apollo-portal
```

##### 4.1.2：清理不用的文件

```shell
root@k8s-master01:/data/k8s-yaml/apollo-adminservice# cd /data/dockerfile/apollo-portal/
root@k8s-master01:/data/dockerfile/apollo-portal#  rm -f apollo-portal-1.7.2-sources.jar
root@k8s-master01:/data/dockerfile/apollo-portal# rm -f apollo-portal.conf
root@k8s-master01:/data/dockerfile/apollo-portal# rm -f scripts/shutdown.sh
```

##### 4.1.3：执行数据库脚本

```shell
root@k8s-master03:~# wget -O apolloportaldb.sql https://raw.githubusercontent.com/ctripcorp/apollo/master/scripts/sql/apolloportaldb.sql

root@k8s-master03:~# mysql -u root -p
mysql> source ./apolloportaldb.sql
```

##### 4.1.4：数据库授权

```mysql
set global validate_password.policy=0;   # Your password does not satisfy the current policy requirements
set global validate_password.length=4;

#创建账户
CREATE USER 'apolloportal'@'172.18.18.%' IDENTIFIED BY '123456';

#赋予权限，with grant option这个选项表示该用户可以将自己拥有的权限授权给别人
grant INSERT,DELETE,UPDATE,SELECT on ApolloPortalDB.* to 'apolloportal'@'172.18.18.%';

#改密码&授权超用户，flush privileges 命令本质上的作用是将当前user和privilige表中的用户信息/权限设置从mysql库(MySQL数据库的内置库)中提取到内存里
flush privileges;

mysql>  update ServerConfig set Value='[{"orgId":"od01","orgName":"Linux学院"},{"orgId":"od02","orgName":"云计算学院"},{"orgId":"od03","orgName":"Python学院"}]' where Id=2;
```

##### 4.2.1：制作Docker镜像

- 配置数据库连接串（用cm的话这里可以不用修改）

```shell
root@k8s-master01:/data/dockerfile/apollo-portal# cat config/application-github.properties
# DataSource
spring.datasource.url = jdbc:mysql://mysql.od.com:3306/ApolloConfigDB?characterEncoding=utf8
spring.datasource.username = apolloportal
spring.datasource.password = 123456
```

- 配置Portal的meta service（用cm的话这里可以不用修改）

```shell
root@k8s-master01:/data/dockerfile/apollo-portal# cat config/apollo-env.properties
dev.meta=http://172.18.18.188:30080
```

##### 4.2.2：更新 startup.sh

```shell
#!/bin/bash
#
# Copyright 2021 Apollo Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
SERVICE_NAME=apollo-portal
## Adjust log dir if necessary
LOG_DIR=/opt/logs/apollo-portal-server
## Adjust server port if necessary
SERVER_PORT=8070
APOLLO_PORTAL_SERVICE_NAME=$(hostname -i)

# SERVER_URL="http://localhost:$SERVER_PORT"
SERVER_URL="http://${APOLLO_PORTAL_SERVICE_NAME}:${SERVER_PORT}"

## Adjust memory settings if necessary
#export JAVA_OPTS="-Xms2560m -Xmx2560m -Xss256k -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=384m -XX:NewSize=1536m -XX:MaxNewSize=1536m -XX:SurvivorRatio=8"

## Only uncomment the following when you are using server jvm
#export JAVA_OPTS="$JAVA_OPTS -server -XX:-ReduceInitialCardMarks"

########### The following is the same for configservice, adminservice, portal ###########
export JAVA_OPTS="$JAVA_OPTS -XX:ParallelGCThreads=4 -XX:MaxTenuringThreshold=9 -XX:+DisableExplicitGC -XX:+ScavengeBeforeFullGC -XX:SoftRefLRUPolicyMSPerMB=0 -XX:+ExplicitGCInvokesConcurrent -XX:+HeapDumpOnOutOfMemoryError -XX:-OmitStackTraceInFastThrow -Duser.timezone=Asia/Shanghai -Dclient.encoding.override=UTF-8 -Dfile.encoding=UTF-8 -Djava.security.egd=file:/dev/./urandom"
export JAVA_OPTS="$JAVA_OPTS -Dserver.port=$SERVER_PORT -Dlogging.file.name=$LOG_DIR/$SERVICE_NAME.log -XX:HeapDumpPath=$LOG_DIR/HeapDumpOnOutOfMemoryError/"

# Find Java
if [[ -n "$JAVA_HOME" ]] && [[ -x "$JAVA_HOME/bin/java" ]]; then
    javaexe="$JAVA_HOME/bin/java"
elif type -p java > /dev/null 2>&1; then
    javaexe=$(type -p java)
elif [[ -x "/usr/bin/java" ]];  then
    javaexe="/usr/bin/java"
else
    echo "Unable to find Java"
    exit 1
fi

if [[ "$javaexe" ]]; then
    version=$("$javaexe" -version 2>&1 | awk -F '"' '/version/ {print $2}')
    version=$(echo "$version" | awk -F. '{printf("%03d%03d",$1,$2);}')
    # now version is of format 009003 (9.3.x)
    if [ $version -ge 011000 ]; then
        JAVA_OPTS="$JAVA_OPTS -Xlog:gc*:$LOG_DIR/gc.log:time,level,tags -Xlog:safepoint -Xlog:gc+heap=trace"
    elif [ $version -ge 010000 ]; then
        JAVA_OPTS="$JAVA_OPTS -Xlog:gc*:$LOG_DIR/gc.log:time,level,tags -Xlog:safepoint -Xlog:gc+heap=trace"
    elif [ $version -ge 009000 ]; then
        JAVA_OPTS="$JAVA_OPTS -Xlog:gc*:$LOG_DIR/gc.log:time,level,tags -Xlog:safepoint -Xlog:gc+heap=trace"
    else
        JAVA_OPTS="$JAVA_OPTS -XX:+UseParNewGC"
        JAVA_OPTS="$JAVA_OPTS -Xloggc:$LOG_DIR/gc.log -XX:+PrintGCDetails"
        JAVA_OPTS="$JAVA_OPTS -XX:+UseConcMarkSweepGC -XX:+UseCMSCompactAtFullCollection -XX:+UseCMSInitiatingOccupancyOnly -XX:CMSInitiatingOccupancyFraction=60 -XX:+CMSClassUnloadingEnabled -XX:+CMSParallelRemarkEnabled -XX:CMSFullGCsBeforeCompaction=9 -XX:+CMSClassUnloadingEnabled  -XX:+PrintGCDateStamps -XX:+PrintGCApplicationConcurrentTime -XX:+PrintHeapAtGC -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=5 -XX:GCLogFileSize=5M"
    fi
fi

printf "$(date) ==== Starting ==== \n"

cd `dirname $0`/..
chmod 755 $SERVICE_NAME".jar"
./$SERVICE_NAME".jar" start

rc=$?;

if [[ $rc != 0 ]];
then
    echo "$(date) Failed to start $SERVICE_NAME.jar, return code: $rc"
    exit $rc;
fi

tail -f /dev/null

chmod a+x startup.sh
```

##### 4.2.3：编辑dockerfile

```dockerfile
root@k8s-master01:/data/dockerfile/apollo-portal# cat Dockerfile 
FROM harbor.od.com/base/jre8:8u112

ENV VERSION 1.7.2

RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime &&\
    echo "Asia/Shanghai" > /etc/timezone

ADD apollo-portal-${VERSION}.jar /apollo-portal/apollo-portal.jar
ADD config/ /apollo-portal/config
ADD scripts/ /apollo-portal/scripts

CMD ["/apollo-portal/scripts/startup.sh"]
```

- 制作镜像并推送

```shell
root@k8s-master01:/data/dockerfile/apollo-portal# docker build . -t harbor.od.com/infra/apollo-portal:v1.7.2
root@k8s-master01:/data/dockerfile/apollo-portal# docker push harbor.od.com/infra/apollo-portal:v1.7.2
```

##### 4.3.1：准备资源配置清单-ConfigMap

```yaml
root@k8s-master01:/data/dockerfile/apollo-portal# mkdir /data/k8s-yaml/apollo-portal && cd /data/k8s-yaml/apollo-portal

root@k8s-master01:/data/k8s-yaml/apollo-portal# cat cm.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: apollo-portal-cm
  namespace: infra
data:
  application-github.properties: |
    # DataSource
    spring.datasource.url = jdbc:mysql://mysql.od.com:3306/ApolloPortalDB?characterEncoding=utf8
    spring.datasource.username = apolloportal
    spring.datasource.password = 123456
  app.properties: |
    appId=100003173
  apollo-env.properties: |
    dev.meta=http://172.18.18.188:30002    # 通过Eureka找到adminservice
```

##### 4.3.2：准备资源配置清单-Deployment

```yaml
root@k8s-master01:/data/k8s-yaml/apollo-portal# cat dp.yaml 
apiVersion: apps/v1
kind: Deployment
metadata:
  name: apollo-portal
  namespace: infra
  labels: 
    name: apollo-portal
spec:
  replicas: 1
  strategy:
    type: RollingUpdate
    rollingUpdate: 
      maxUnavailable: 1
      maxSurge: 1
  revisionHistoryLimit: 10
  progressDeadlineSeconds: 600
  selector:
    matchLabels: 
      name: apollo-portal
  template:
    metadata:
      labels: 
        app: apollo-portal 
        name: apollo-portal
    spec:
      restartPolicy: Always
      terminationGracePeriodSeconds: 60
      hostAliases:
      - ip: "172.18.18.20"
        hostnames:
        - "mysql.od.com"
      containers:        
        - name: apollo-portal
          image: harbor.od.com/infra/apollo-portal:v1.7.2
          imagePullPolicy: IfNotPresent
          terminationMessagePath: /dev/termination-log
          terminationMessagePolicy: File
          securityContext: 
            runAsUser: 0
          ports:
            - containerPort: 8070
              protocol: TCP
          volumeMounts:
            - name: configmap-volume
              mountPath: /apollo-portal/config
                
          readinessProbe:
            tcpSocket:
              port: 8070
            initialDelaySeconds: 10
            periodSeconds: 5

          livenessProbe:
            tcpSocket:
              port: 8070
            initialDelaySeconds:  120
            periodSeconds: 10

      volumes:
        - name: configmap-volume
          configMap:
            name: apollo-portal-cm
```

##### 4.3.3：准备资源配置清单-Service

```yaml
root@k8s-master01:/data/k8s-yaml/apollo-portal# cat svc.yaml
apiVersion: v1
kind: Service
metadata: 
  name: apollo-portal
  namespace: infra
spec:
  ports:
  - protocol: TCP
    port: 8070
    targetPort: 8070
    nodePort: 30080
  selector: 
    app: apollo-portal
  type: NodePort
```

##### 4.3.4：声明式方式创建资源对象

```shell
root@k8s-master01:/data/k8s-yaml/apollo-portal# kubectl apply -f cm.yaml
root@k8s-master01:/data/k8s-yaml/apollo-portal# kubectl apply -f dp.yaml
root@k8s-master01:/data/k8s-yaml/apollo-portal# kubectl apply -f svc.yaml
```

##### 4.3.5：浏览器访问验证

```http
http://172.18.18.188:30080/

用户名：apollo
密码： admin
```

#### 五：通过ingress的方式,更改configservice以及portal

##### 5.1：修改数据等操作

```yaml
update ApolloConfigDB.ServerConfig set ServerConfig.Value="http://config.od.com/eureka" where ServerConfig.Key="eureka.service.url";

MariaDB [ApolloConfigDB]> select * from ServerConfig\G
*************************** 1. row ***************************
                       Id: 1
                      Key: eureka.service.url
                  Cluster: default
                    Value: http://config.od.com/eureka
                  Comment: Eureka服务Url，多个service以英文逗号分隔
                IsDeleted:  
     DataChange_CreatedBy: default
   DataChange_CreatedTime: 2019-12-10 16:08:44
DataChange_LastModifiedBy: 
      DataChange_LastTime: 2019-12-10 16:13:53
```

##### 5.2：修改configservice的相关配置

```yaml
configmap/apollo-configservice-cm configured
root@k8s-master01:/data/k8s-yaml/apollo-configservice# cat cm.yaml 
apiVersion: v1
kind: ConfigMap
metadata:
  name: apollo-configservice-cm
  namespace: infra
data:
  application-github.properties: |
    # DataSource
    spring.datasource.url = jdbc:mysql://mysql.od.com:3306/ApolloConfigDB?characterEncoding=utf8
    #spring.datasource.url = jdbc:mysql://172.18.18.20:3306/ApolloConfigDB?characterEncoding=utf8
    spring.datasource.username = apolloconfig
    spring.datasource.password = 123456
    eureka.service.url = http://config.od.com/eureka 
  app.properties: |
    appId=100003171
    
root@k8s-master01:/data/k8s-yaml/apollo-configservice# cat svc.yaml
kind: Service
apiVersion: v1
metadata: 
  name: apollo-configservice
  namespace: infra
spec:
  ports:
  - protocol: TCP
    port: 8080
    targetPort: 8080
  selector: 
    app: apollo-configservice    
    

root@k8s-master01:/data/k8s-yaml/apollo-configservice# cat ingress.yaml 
apiVersion: networking.k8s.io/v1beta1    
kind: Ingress
metadata: 
  annotations:
    kubernetes.io/ingress.class: "nginx"
  namespace: infra  
  name: apollo-configservice
spec:
  rules:
  - host: config.od.com
    http:
      paths:
      - path: /
        backend: 
          serviceName: apollo-configservice
          servicePort: 8080
          
root@k8s-master01:/data/k8s-yaml/apollo-configservice# cat dp.yaml 
apiVersion: apps/v1
kind: Deployment
metadata:
  name: apollo-configservice
  namespace: infra
  labels: 
    name: apollo-configservice
spec:
  replicas: 1
  strategy:
    type: RollingUpdate
    rollingUpdate: 
      maxUnavailable: 1
      maxSurge: 1
  revisionHistoryLimit: 10
  progressDeadlineSeconds: 600
  selector:
    matchLabels: 
      name: apollo-configservice
  template:
    metadata:
      labels: 
        app: apollo-configservice 
        name: apollo-configservice
    spec:
      restartPolicy: Always
      terminationGracePeriodSeconds: 60
      hostAliases:
      - ip: "172.18.18.20"
        hostnames:
        - "mysql.od.com"
      - ip: "172.18.18.18"    # 需要添加ingress节点的地址，如果是真实域名无需此配置
        hostnames:
        - "config.od.com"
      containers:        
        - name: apollo-configservice
          image: harbor.od.com/infra/apollo-configservice:v1.7.2
          imagePullPolicy: IfNotPresent
          #terminationMessagePath: /dev/termination-log
          #terminationMessagePolicy: File
          #securityContext: 
          #  runAsUser: 0
          ports:
            - containerPort: 8080
              protocol: TCP
          volumeMounts:
            - name: configmap-volume
              mountPath: /apollo-configservice/config
                
          readinessProbe:
            tcpSocket:
              port: 8080
            initialDelaySeconds: 10
            periodSeconds: 5

          livenessProbe:
            tcpSocket:
              port: 8080
            initialDelaySeconds:  120
            periodSeconds: 10

      volumes:
        - name: configmap-volume
          configMap:
            name: apollo-configservice-cm
    

root@k8s-master01:/data/k8s-yaml/apollo-configservice# kubectl apply -f cm.yaml  
root@k8s-master01:/data/k8s-yaml/apollo-configservice# kubectl apply -f svc.yaml
root@k8s-master01:/data/k8s-yaml/apollo-configservice# kubectl apply -f ingress.yaml
root@k8s-master01:/data/k8s-yaml/apollo-configservice# kubectl delete -f dp.yaml
root@k8s-master01:/data/k8s-yaml/apollo-configservice# kubectl apply -f dp.yaml
```

##### 5.2：修改adminservice的相关配置

```yaml
root@k8s-master01:/data/k8s-yaml/apollo-adminservice# cat cm.yaml 
apiVersion: v1
kind: ConfigMap
metadata:
  name: apollo-adminservice-cm
  namespace: infra
data:
  application-github.properties: |
    # DataSource
    spring.datasource.url = jdbc:mysql://mysql.od.com:3306/ApolloConfigDB?characterEncoding=utf8
    spring.datasource.username = apolloconfig
    spring.datasource.password = 123456
  eureka.service.url = http://config.od.com/eureka
  app.properties: |
    appId=100003172
    
    
root@k8s-master01:/data/k8s-yaml/apollo-adminservice# cat dp.yaml 
apiVersion: apps/v1
kind: Deployment
metadata:
  name: apollo-adminservice
  namespace: infra
  labels: 
    name: apollo-adminservice
spec:
  replicas: 1
  strategy:
    type: RollingUpdate
    rollingUpdate: 
      maxUnavailable: 1
      maxSurge: 1
  revisionHistoryLimit: 10
  progressDeadlineSeconds: 600
  selector:
    matchLabels: 
      name: apollo-adminservice
  template:
    metadata:
      labels: 
        app: apollo-adminservice 
        name: apollo-adminservice
    spec:
      restartPolicy: Always
      terminationGracePeriodSeconds: 60
      hostAliases:
      - ip: "172.18.18.20"
        hostnames:
        - "mysql.od.com"
      - ip: "172.18.18.18"
        hostnames:
        - "config.od.com"
      containers:        
        - name: apollo-adminservice
          image: harbor.od.com/infra/apollo-adminservice:v1.7.2
          imagePullPolicy: IfNotPresent
          terminationMessagePath: /dev/termination-log
          terminationMessagePolicy: File
          securityContext: 
            runAsUser: 0
          ports:
            - containerPort: 8080
              protocol: TCP
          volumeMounts:
            - name: configmap-volume
              mountPath: /apollo-adminservice/config
                
          readinessProbe:
            tcpSocket:
              port: 8080
            initialDelaySeconds: 10
            periodSeconds: 5

          livenessProbe:
            tcpSocket:
              port: 8080
            initialDelaySeconds:  120
            periodSeconds: 10

      volumes:
        - name: configmap-volume
          configMap:
            name: apollo-adminservice-cm   
            
root@k8s-master01:/data/k8s-yaml/apollo-adminservice# kubectl apply -f cm.yaml
root@k8s-master01:/data/k8s-yaml/apollo-adminservice# kubectl apply -f dp.yaml
```

##### 5.3：修改portal配置清单

```yaml
root@k8s-master01:/data/k8s-yaml/apollo-portal# cat cm.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: apollo-portal-cm
  namespace: infra
data:
  application-github.properties: |
    # DataSource
    spring.datasource.url = jdbc:mysql://mysql.od.com:3306/ApolloPortalDB?characterEncoding=utf8
    spring.datasource.username = apolloportal
    spring.datasource.password = 123456
  app.properties: |
    appId=100003173
  apollo-env.properties: |
    dev.meta=http://config.od.com
    


root@k8s-master01:/data/k8s-yaml/apollo-portal# cat dp.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: apollo-portal
  namespace: infra
  labels: 
    name: apollo-portal
spec:
  replicas: 1
  strategy:
    type: RollingUpdate
    rollingUpdate: 
      maxUnavailable: 1
      maxSurge: 1
  revisionHistoryLimit: 10
  progressDeadlineSeconds: 600
  selector:
    matchLabels: 
      name: apollo-portal
  template:
    metadata:
      labels: 
        app: apollo-portal 
        name: apollo-portal
    spec:
      restartPolicy: Always
      terminationGracePeriodSeconds: 60
      hostAliases:
      - ip: "172.18.18.20"
        hostnames:
        - "mysql.od.com"
      - ip: "172.18.18.18"
        hostnames:
        - "config.od.com"
      containers:        
        - name: apollo-portal
          image: harbor.od.com/infra/apollo-portal:v1.7.2
          imagePullPolicy: IfNotPresent
          terminationMessagePath: /dev/termination-log
          terminationMessagePolicy: File
          securityContext: 
            runAsUser: 0
          ports:
            - containerPort: 8070
              protocol: TCP
          volumeMounts:
            - name: configmap-volume
              mountPath: /apollo-portal/config
                
          readinessProbe:
            tcpSocket:
              port: 8070
            initialDelaySeconds: 10
            periodSeconds: 5

          livenessProbe:
            tcpSocket:
              port: 8070
            initialDelaySeconds:  120
            periodSeconds: 10

      volumes:
        - name: configmap-volume
          configMap:
            name: apollo-portal-cm   
            
            
root@k8s-master01:/data/k8s-yaml/apollo-portal# cat ingress.yaml 
apiVersion: networking.k8s.io/v1beta1    
kind: Ingress
metadata: 
  annotations:
    kubernetes.io/ingress.class: "nginx"
  namespace: infra  
  name: apollo-portal
spec:
  rules:
  - host: portal.od.com
    http:
      paths:
      - path: /
        backend: 
          serviceName: apollo-portal
          servicePort: 8070


root@k8s-master01:/data/k8s-yaml/apollo-portal# cat svc.yaml 
apiVersion: v1
kind: Service
metadata: 
  name: apollo-portal
  namespace: infra
spec:
  ports:
  - protocol: TCP
    port: 8070
    targetPort: 8070
  selector: 
    app: apollo-portal
    
root@k8s-master01:/data/k8s-yaml/apollo-portal# kubectl apply -f .
configmap/apollo-portal-cm configured
deployment.apps/apollo-portal configured
Warning: networking.k8s.io/v1beta1 Ingress is deprecated in v1.19+, unavailable in v1.22+; use networking.k8s.io/v1 Ingress
ingress.networking.k8s.io/apollo-portal created
service/apollo-portal configured    
```

##### 5.4：解析域名并做相关验证

```http
172.18.18.18 config.od.com portal.od.com    # 在自己电脑做解析
http://portal.od.com/
http://config.od.com/

APOLLO-ADMINSERVICE	n/a (1)	(1)	UP (1) - apollo-adminservice-8588b546d9-247xz:apollo-adminservice:8080
APOLLO-CONFIGSERVICE	n/a (1)	(1)	UP (1) - apollo-configservice-6d7c94c84b-dn5bl:apollo-configservice:8080
```

